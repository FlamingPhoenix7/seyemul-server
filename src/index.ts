import express from "express";
import { createServer } from "http";
import { Server } from "socket.io";

const app = express();
const PORT = process.env.PORT || 8000;
const httpServer = createServer(app);

app.use("/", express.static("./public"));

const io = new Server(httpServer, {
  cors: {
    origin: process.env.NODE_ENV === "dev" ? "*" : "https://seyemul.vercel.app",
    methods: ["GET", "POST"],
    credentials: true,
  },
});

io.on("connection", (socket) => {
  const user = {
    id: socket.id,
    username: null,
    label: () => user.username || user.id,
  };

  socket.send({
    timeStamp: Date.now(),
    text: `Hello, socket ${user.id}`,
  });
  socket.broadcast.emit("message", {
    timeStamp: Date.now(),
    text: `Socket ${user.id} joined the party, yay!!!`,
  });

  socket.on("message", (message) => {
    console.log(`Message from ${user.label()}:`);
    console.log(message);
  });
});

httpServer.listen(PORT, () => {
  console.log(`NODE_ENV = ${process.env.NODE_ENV}`);
  console.log(`Server started on port ${PORT}`);
});
