# Seyemul

## The platform for watching YouTube videos with a common shared control

This is the repo for the site's server. The actual site is live [here](https://seyemul.vercel.app)
